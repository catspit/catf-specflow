﻿using ClickPortal.Pages;
using ClickPortal.PortalDriver;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace CATFSpecFlow
{
    public class TestContext : IDisposable
    {
        public PortalDriver Driver { get; set; }

        public TestContext()
        {
            // Set the current directory so that relative paths work.
            Assembly current = Assembly.GetExecutingAssembly();
            DirectoryInfo locationInfo = Directory.GetParent(current.Location);
            String config = System.IO.File.ReadAllText(Path.Combine(locationInfo.ToString(), "storeconfig.json"));

            Driver = new PortalDriver(config);
            PageController.driver = Driver;
        }

        public void Dispose()
        {
            Driver.Quit();
        }
    }
}
