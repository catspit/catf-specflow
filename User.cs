﻿using ClickPortal.PortalDriver;
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace CATFSpecFlow
{
    [Binding]
    public sealed class User
    {
        TestContext context;
        PortalDriver Driver;

        public User(TestContext context)
        {
            this.context = context;
            this.Driver = context.Driver;
        }

        [Given(@"I am logged into the system as ""(.*)""")]
        [When(@"I log in as ""(.*)""")]
        public void LoginAsUser(string userid)
        {
            if (userid == "administrator")
            {
                Driver.Store().LoginAdmin();
            }
            else
            {
                Driver.Store().Login(userid, "BabyShark!");
            }
        }

        [Then(@"I expect to see a profile link with text ""(.*)""")]
        public void IExpectToSeeAProfileLinkWithText(string linktext)
        {
            IWebElement profileLink;
            if (Driver.Store().portalVersionMajor == 8) {
                profileLink = Driver.FindElement(By.Id("AccountDropDownLink"));
            } else
            {
                profileLink = Driver.FindElement(By.XPath(@"//a[contains(@href, 'MyProfile')]"));
            }

            Assert.AreEqual(linktext, profileLink.Text, "Profile link text is incorrect.");
        }
    }
}
