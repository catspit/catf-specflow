﻿using ClickPortal.Pages;
using ClickPortal.PortalDriver;
using NUnit.Framework;
using OpenQA.Selenium;
using TechTalk.SpecFlow;

namespace CATFSpecFlow.PageSteps
{
    [Binding]
    public sealed class SmartformSteps
    {
        TestContext context;
        PortalDriver Driver;
        public SmartformSteps(TestContext context)
        {
            this.context = context;
            this.Driver = context.Driver;
        }
        [When(@"I click Continue button in Smartform Page")]
        public void WhenIClickContinueButtonInSmartformPage()
        {
            SmartFormPage.Continue();
        }
        [When(@"I jump to ""(.*)"" page")]
        public void WhenIJumpToPage(string sfName)
        {
            SmartFormPage.JumpTo(sfName);
        }
        [When(@"I save the smartform page")]
        public void WhenISaveTheSmartformPage()
        {
            SmartFormPage.Save();
        }
        [When(@"I exit the smartform page")]
        public void WhenIExitTheSmartformPage()
        {
            SmartFormPage.Exit();
        }
        [When(@"I save and exit the smartform page")]
        public void WhenISaveAndExitTheSmartformPage()
        {
            SmartFormPage.SaveAndExit();
        }
        [Then(@"I expect to see ""(.*)"" instead of the Finish button")]
        public void ThenIExpectToSeeInsteadOfTheFinishButton(string expectedButtonText)
        {
            IWebElement finishButton = SmartFormPage.GetFinishButton();
            string actualButtonText = finishButton.GetAttribute("value");
            Assert.AreEqual(actualButtonText, expectedButtonText);
        }

    }
}
