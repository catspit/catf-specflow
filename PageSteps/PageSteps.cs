﻿using ClickPortal.PortalDriver;
using NUnit.Framework;
using OpenQA.Selenium;
using TechTalk.SpecFlow;
namespace CATFSpecFlow.PageSteps
{
    [Binding]
    public sealed class PageSteps
    {
        TestContext context;
        PortalDriver Driver;
        public PageSteps(TestContext context)
        {
            this.context = context;
            this.Driver = context.Driver;
        }
        [When(@"I click the project creator button ""(.*)""")]
        public void WhenIClickTheProjectCreatorButton(string projectCreatorName)
        {
            Driver.ProjectCreator(projectCreatorName).Click();
        }
        [When(@"I click on the ""(.*)"" link to open new window")]
        public void WhenIClickOnTheLinkToOpenNewWindow(string linkText)
        {
            IWebElement elem = Driver.FindElement(By.LinkText(linkText));
            Driver.Window().New(elem.Click);
        }
        [When(@"I click the My Profile link from the account drop down link")]
        public void WhenIClickTheMyProfileLinkFromTheAccountDropDownLink()
        {
            ClickAccountDropDownLink();
            const string ID_MY_PROFILE_LINK = "MyProfileLink";
            Driver.FindElement(By.Id(ID_MY_PROFILE_LINK)).Click();
        }
        private void ClickAccountDropDownLink()
        {
            const string ID_ACCOUNT_DROP_DOWN_LINK = "AccountDropDownLink";
            Driver.FindElement(By.Id(ID_ACCOUNT_DROP_DOWN_LINK)).Click();
        }
        [Then(@"I expect to see the ""(.*)"" field that is readonly")]
        public void ThenIExpectToSeeTheFieldThatIsReadonly(string caption)
        {
            IPortalControl field = Driver.Control(caption);
            string actualType = field.Type;
            string expectedType = "readOnly";
            Assert.AreEqual(actualType, expectedType);
        }
    }
}
