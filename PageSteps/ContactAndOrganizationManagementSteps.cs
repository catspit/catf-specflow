﻿using ClickPortal.Pages;
using ClickPortal.PortalDriver;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using TechTalk.SpecFlow;

namespace CATFSpecFlow
{
    [Binding]
    public sealed class ContactAndOrganizationManagementSteps
    {
        TestContext context;
        PortalDriver Driver;
        ContactAndOrganizationManagementPage ContactAndOrganizationManagement;

        public ContactAndOrganizationManagementSteps(TestContext context)
        {
            this.context = context;
            this.Driver = context.Driver;
            this.ContactAndOrganizationManagement = new ContactAndOrganizationManagementPage();
        }

        [When(@"I navigate to Contact and Organization Management Page")]
        public void WhenINavigateToContactAndOrganizationManagementPage()
        {
            this.ContactAndOrganizationManagement.Navigate();
        }
        [Then(@"I expect to see ""(.*)"" for the Dashboard Template")]
        public void ThenIExpectToSeeForTheDashboardTemplate(string expectedTemplateName)
        {
            string actualTemplateName = this.ContactAndOrganizationManagement.GetDashboardTemplate();
            Assert.AreEqual(actualTemplateName, expectedTemplateName);
        }

        [When(@"I select view ""(.*)"" from the properties tab")]
        public void WhenISelectViewFromThePropertiesTab(string viewName)
        {
            SelectElement select = new SelectElement(Driver.FindElement(By.Id("PropertiesViewSelector")));
            select.SelectByText(viewName);
        }
    }
}
