﻿
using ClickPortal.PortalDriver;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Collections.ObjectModel;
using TechTalk.SpecFlow;

namespace CATFSpecFlow.PageSteps
{
    [Binding]
    public sealed class ChooserSteps
    {
        TestContext context;
        PortalDriver Driver;
        const string ID_DISPLAY_FIELDS = "rsvDisplayFields";
        const string ID_FILTER_ONLY_FIELDS = "rsvFilterOnlyFields";
        public ChooserSteps(TestContext context)
        {
            this.context = context;
            this.Driver = context.Driver;
        }
        IWebElement DisplayFieldsTable
        {
            get
            {
                return Driver.FindElement(By.Id(ID_DISPLAY_FIELDS));
            }
        }
        IWebElement FilterOnlyFieldsTable
        {
            get
            {
                return Driver.FindElement(By.Id(ID_FILTER_ONLY_FIELDS));
            }
        }
        [Then(@"I should see the display field ""(.*)""")]
        public void ThenIShouldSeeTheDisplayField(string displayText)
        {
            ReadOnlyCollection<IWebElement> fieldsResult = DisplayFieldsTable.FindElements(By.XPath(".//td[@class='List']/span[text()='" + displayText + "']"));
            Assert.AreEqual(1, fieldsResult.Count);
        }
        [Then(@"I should see the filter-only field ""(.*)""")]
        public void ThenIShouldSeeTheFilter_OnlyField(string displayText)
        {
            ReadOnlyCollection<IWebElement> fieldsResult = FilterOnlyFieldsTable.FindElements(By.XPath(".//td[@class='List']/span[text()='" + displayText + "']"));
            Assert.AreEqual(1, fieldsResult.Count);
        }


    }
}
