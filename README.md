﻿# CATF-Specflow

This project contains a collection of common SpecFlow test steps for testing
Click® Portal based applications, including specific products such as Click® IRB7 or IACUC7.

## How to use this project
1. Add this project to your test solution and add a reference to it from your test library project.
2. Modify your app.config file and add "CATF-SpecFlow" as an external step assembly (https://github.com/techtalk/SpecFlow/wiki/Use-Bindings-from-External-Assemblies).
3. Add a storeconfig.json file to your project which contains the PortalDriver configuration string (https://wiki.womoverflow.com/index.php?title=Community_Automated_Testing_Framework/PortalDriver#Config_String)
4. Use the test steps in your SpecFlow feature files