﻿using System;
using System.Collections.Generic;
using System.IO;
using ClickPortal.Pages;
using ClickPortal.PortalDriver;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.Reflection;
using ClickPortal;
using NUnit.Framework;

namespace CATFSpecFlow
{
    public abstract class BaseTest
    {
        protected PortalDriver Driver;
        protected string BaseURL;
        protected List<String> BulkImportSetup = new List<String>();
        protected List<String> BulkImportTeardown= new List<String>();

        protected List<String> PreImportScripts= new List<String>();
        protected List<String> PostImportScripts= new List<String>();
        protected List<String> PreTeardownScripts= new List<String>();
        protected List<String> PostTeardownScripts= new List<String>();

        protected TestDataList TestData;

        protected BaseTest()
        {
        }

        protected BaseTest(String options)
        {
            this.Initialize(options);
        }

        protected void Initialize(String config)
        {
            Driver = new PortalDriver(config); 
            PageController.driver = Driver;
            TestData = new TestDataList(Driver);

            // Set the current directory so that relative paths work.
            Assembly current = Assembly.GetExecutingAssembly();
            DirectoryInfo locationInfo = Directory.GetParent(current.Location);
            Directory.SetCurrentDirectory(locationInfo.ToString());
        }

        public bool HasSetupSteps
        {
            get
            {
                return PreImportScripts.Count > 0 ||
                       PreTeardownScripts.Count > 0 ||
                       BulkImportSetup.Count > 0 ||
                       TestData.Count > 0;
            }
        }

        public bool HasTeardownSteps
        {
            get
            {
                return PostTeardownScripts.Count > 0 ||
                       PostImportScripts.Count > 0 ||
                       BulkImportTeardown.Count > 0 ||
                       TestData.Count > 0;
            }
        }

        protected virtual void RunTestFixtureSetup()
        {

            NavigateToBaseUrl();

            if (HasSetupSteps == false)
            {
                // No need to login if nothing to import
                return;                
            }

            if (PreImportScripts.Count > 0)
            {
                ExecuteCommandConsoleScriptList(PreImportScripts);
            }

            if (BulkImportSetup.Count > 0)
            {
                RunBulkImports(BulkImportSetup);
            }

            if (PostImportScripts.Count > 0)
            {
                ExecuteCommandConsoleScriptList(PostImportScripts);
            }
        }

        private void NavigateToBaseUrl()
        {
            Driver.Navigate().GoToUrl(Driver.getBaseURL(null));
        }


        private void StepExecuteCommandConsoleScriptList() {
            if (PreTeardownScripts.Count > 0) ExecuteCommandConsoleScriptList(PreTeardownScripts);
        }

        private void StepDestoryTestDataEntities()
        {
            if (TestData.Count == 0) return;
            foreach (ClickEType entity in TestData) entity.DestroyEntity(Driver);
        }

        private void StepBulkImportTeardown()
        {
            if (BulkImportTeardown.Count > 0) RunBulkImports(BulkImportTeardown);
        }

        private void StepPostTearDownScripts() {
            if (PostTeardownScripts.Count > 0) ExecuteCommandConsoleScriptList(PostTeardownScripts);
        }

        private void DriverQuit() {
            Driver.Quit();
        }


        protected virtual void RunTestFixtureTeardown()
        {
            //This is a sufficient .Reset() stand-in for now
            try
            {
                Driver.Navigate().GoToUrl("about:blank");
            }
            finally {
                //Driver.Navigate().GoToUrl("about:blank");
            }

            var tearDownSteps = new List<Action> {
                StepExecuteCommandConsoleScriptList,
                StepDestoryTestDataEntities,
                StepBulkImportTeardown,
                StepPostTearDownScripts,
                DriverQuit
            };

            var errorReport = "";

            foreach (var step in tearDownSteps) {
                try {
                    step();
                }
                catch (Exception e){
                    errorReport += "TEARDOWN STEP FAILURE: executing step: " + step.Method.Name + "\n";
                    errorReport += "   step threw the following exception: " + e.Message + "\n";
                    errorReport += "   trace: \n" + e.StackTrace + "\n\n";
                }
            }
            if (!errorReport.Equals("")) {
                NUnit.Framework.TestContext.Progress.WriteLine(errorReport);
                NUnit.Framework.TestContext.Out.WriteLine(errorReport);
                NUnit.Framework.TestContext.WriteLine(errorReport);
                Assert.Fail(errorReport);
            }
        }

        private void RunBulkImports(List<String> listOfSheets)
        {
            Driver.Store().LoginAdmin();
            foreach (String filepath in listOfSheets)
            {
                BulkImportPage biPage = new BulkImportPage();
                biPage.Navigate();
                biPage.Upload(filepath, true);
            }
        }

        private void ExecuteCommandConsoleScriptList(List<String> listOfScriptFiles)
        {
            foreach (String filepath in listOfScriptFiles)
            {
                String script = File.ReadAllText(filepath);
                Driver.Store().Execute(script);
            }
        }
       
        protected class TestDataList : List<ClickEType>
        {
            PortalDriver Driver;

            public TestDataList(PortalDriver Driver) : base()
            {
                this.Driver = Driver;
            }

            public new void Add(ClickEType project)
            {
                base.Add(project);
                project.CreateEntity(Driver);
            }
        }
    }
 
}