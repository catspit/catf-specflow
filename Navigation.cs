﻿using ClickPortal.Pages;
using ClickPortal.PortalDriver;
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace CATFSpecFlow
{
    [Binding]
    public sealed class Navigation
    {
        TestContext context;
        PortalDriver Driver;

        public Navigation(TestContext context)
        {
            this.context = context;
            this.Driver = context.Driver;
        }
        [When(@"I click ""(.*)"" from the top navigator")]
        public void WhenIClickFromTheTopNavigator(string pageName)
        {
            Driver.TopNavTab(pageName).Click();
        }

        [Then(@"I expect to see the Unauthorized Access page")]
        public void ThenIExpectToSeeTheUnauthorizedAccessPage()
        {
            Assert.IsTrue(Driver.Url.EndsWith("DisplayPages/UnauthorizedAccess"), "Resulting page not recognized: {0}", Driver.Url);
        }
    }
}
